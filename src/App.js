import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import NavMenu from './components/NavMenu';
import Author from './pages/Author';
import Home from './pages/Home';
import Article from './pages/Article';
import Catagory from './pages/Catagory'
function App() {
	return (
		<Router>
			<NavMenu />
			<Container>
				<Switch>
					{/* <Route exact path='/' component={Home} />
					<Route path='/detail/:id' component={ArticleDetail} />
					<Route path='/video' component={Video} />
					<Route path='/account' component={Account} />
					<ProtectedRoute path='/welcome'>
						<Welcome />
					</ProtectedRoute> */}
					<Route exact path='/' component={Home} />
					<Route path='/author' component={Author} />
					<Route path='/article' component={Article} />
					<Route path='/category' component={Catagory} />
				</Switch>
			</Container>
		</Router>
	);
}

export default App;
