import api from '../utils/apis';

export const fetchCatagory = async () => {
	let response = await api.get('/category');
	return response.data.data;
};

export const deleteCatagory = async (id) => {
	let response = await api.delete('catagory/' + id);
	return response.data.message;
};

export const postCatagory = async (catagory) => {
	let response = await api.post('/catagory', catagory);
	return response.data.data;
};

export const updateCatagoryById = async (id, updatedCatagory) => {
	let response = await api.put('/catagory/' + id, updatedCatagory);
	return response.data.message;
};

