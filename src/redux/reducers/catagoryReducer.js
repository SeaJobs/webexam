import {catagoryActionType} from '../actions/catagoryActionType'

const initialState = {
	catagory: [],
};

const catagoryReducer = (state = initialState, action) => {
	switch (action.type) {
		case catagoryActionType.FETCH_CATAGORY:
			return { ...state, catagory: [...action.payload] };
		case catagoryActionType.INSERT_CATAGORY:
			return { ...state, catagory: [action.payload, ...state.catagory] };

		case catagoryActionType.DELETE_CATAGORY:
			return { ...state, catagory: state.authors.filter((catagory) => catagory._id != action.payload) };

		case catagoryActionType.UPDATE_CATAGORY:
			console.log(action);
			let newCatagory = [...state.catagory];
			newCatagory = newCatagory.map((catagory) => {
				if (catagory._id === action.payload.authorId) {
					catagory.name = action.payload.updatedCatagory.name;
				}
				return catagory;
			});

			return { ...state, catagory: newCatagory };

		default:
			return state;
	}
};

export default catagoryReducer;
