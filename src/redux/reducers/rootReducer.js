import { combineReducers } from 'redux';
import articleReducer from './articleReducer';
import authorReducer from './authorReducer';
import catagoryReducer from './catagoryReducer';

const rootReducer = combineReducers({
    authors: authorReducer,
    catagory: catagoryReducer,
    article: articleReducer
});

export default rootReducer;

