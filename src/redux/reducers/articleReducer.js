import { articleActionType } from '../actions/articleActionType';

const initialState = {
	article: [],
};

const articleReducer = (state = initialState, action) => {
	switch (action.type) {
		case articleActionType.FETCH_ARTICLE:
			return { ...state, article: [...action.payload] };
		case articleActionType.INSERT_AUTHOR:
			return { ...state, article: [action.payload, ...state.article] };

		case articleActionType.DELETE_ARTICLE:
			return { ...state, article: state.article.filter((article) => article._id != action.payload) };

		case articleActionType.UPDATE_ARTICLE:
			console.log(action);
			let newArticle = [...state.article];
			newArticle = newArticle.map((article) => {
				if (article._id === action.payload.authorId) {
					article.name = action.payload.updatedArticle.name;
					article.email = action.payload.updatedArticle.email;
					article.image = action.payload.updatedArticle.image;
				}
				return article;
			});

			return { ...state, authors: newArticle };

		default:
			return state;
	}
};

export default articleReducer;
