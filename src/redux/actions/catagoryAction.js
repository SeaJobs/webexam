import { fetchCatagory, postCatagory, deleteCatagory, updateCatagoryById } from '../../services/catagoryService';
import {catagoryActionType} from '../actions/catagoryActionType';

export const onFetchCatagory = () => async (dispatch) => {
	let catagory = await fetchCatagory();
	dispatch({
		type: catagoryActionType.FETCH_CATAGORY,
		payload: catagory,
	});
};

export const onInsertCatagory = (newCatagory) => async (dispatch) => {
	let catagory = await postCatagory(newCatagory);

	dispatch({
		type: catagoryActionType.INSERT_CATAGORY,
		payload: catagory,
	});
};

export const onDeleteCatagory = (catagoryId) => async (dispatch) => {
	let message = await deleteCatagory(catagoryId);

	dispatch({
		type: catagoryActionType.DELETE_CATAGORY,
		payload: catagoryId,
	});
};

export const onUpdateCatagory = (catagoryId, updatedCatagory) => async (dispatch) => {
	let message = await updateCatagoryById(catagoryId, updatedCatagory);

	dispatch({
		type: catagoryActionType.UPDATE_CATAGORY,
		payload: { catagoryId, updatedCatagory },
	});
};
