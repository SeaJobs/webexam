import { fetchArticle, postArticle, deleteArticle, updateArticleById } from '../../services/article_service';
import { articleActionType } from '../actions/articleActionType';

export const onFetchArticle = () => async (dispatch) => {
	let articles = await fetchArticle();
	dispatch({
		type: articleActionType.FETCH_ARTICLE,
		payload: articles,
	});
};

export const onInsertArticle = (newArticle) => async (dispatch) => {
	let article = await postArticle(newArticle);

	dispatch({
		type: articleActionType.INSERT_ARTICLE,
		payload: article,
	});
};

export const onDeleteArticle = (articleId) => async (dispatch) => {
	let message = await deleteArticle(articleId);

	dispatch({
		type: articleActionType.DELETE_ARTICLE,
		payload: articleId,
	});
};

export const onUpdateArticle = (articleId, updatedArticle) => async (dispatch) => {
	let message = await updateArticleById(articleId, updatedArticle);

	dispatch({
		type: articleActionType.UPDATE_ARTICLE,
		payload: { articleId, updatedArticle },
	});
};
