
import React, { useEffect, useState } from 'react';
import { Container, Button, Col, Row, Form , Card} from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { onFetchAuthors, onInsertAuthor, onUpdateAuthor } from '../redux/actions/authorAction';
import {onFetchCatagory} from '../redux/actions/catagoryAction';
import { uploadImage, fetchArticle,deleteArticle} from '../services/article_service';
import {useHistory} from 'react-router';
import { fetchCatagory } from '../services/catagoryService';
import ReactLoading from 'react-loading'

function Home() {
	const [authorName, setAuthorName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');
	const [catagoryName, setcatagoryName] = useState('');

	// const authors = useSelector((state) => state.authors.authors);
	const dispatch = useDispatch();

	const loading = ({ type, color }) => (
		<ReactLoading type={type} color={color} height={667} width={375} />
	);
	 


	const [articles, setArticles] = useState([]);

	useEffect(() => {
	  const fetch = async () => {
		let articles = await fetchArticle();
		setArticles(articles);

		const catagoryFetch = async () => {
			let catagory = await fetchCatagory();
			setcatagoryName(catagory);
		}
	  };
	  fetch();
	}, []);
  
	const history = useHistory()
  
  
	const onDelete = (id)=>{
	  deleteArticle(id).then((message)=>{
  
		let newArticles = articles.filter((article)=>article._id !== id)
		setArticles(newArticles)
  
		alert(message)
	  }).catch(e=>console.log(e))
	}

	
	let articleCard = articles.map((article) => (
		
		<Col md={3} key={article._id}>
		  <Card className="my-2" style = {{}}>
			<Card.Img
			  variant="top"
			  style={{ objectFit: "cover", height: "130px"}}
			  src={article.image ? article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
			/>
			<Card.Body>
			  <Card.Title>{article.title}</Card.Title>
			  <Card.Text className="text-line-3">
			   {article.description}
			  </Card.Text>
			  <Button 
				size="sm" 
				variant="primary"
				onClick={()=>
				  history.push('/article/'+article._id)
				}
			  >
				Read
			  </Button>{" "}
			  <Button 
				size="sm" 
				variant="warning"
				onClick={()=>{
				  history.push('/update/article/'+article._id)
				}}
			  >
				Edit
			  </Button>{" "}
			  <Button size="sm" variant="danger" onClick={()=>onDelete(article._id)}>
				Delete
			  </Button>
			</Card.Body>
		  </Card>
		</Col>
	  ));


	useEffect(() => {
		dispatch(onFetchAuthors());
		dispatch(onFetchCatagory);
	}, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newAuthor = {
			name: authorName,
			email,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			newAuthor.image = url;
		} else {
			newAuthor.image = imageURL;
		}

		if (selectedId) {
			dispatch(onUpdateAuthor(selectedId, newAuthor));
		} else {
			
			dispatch(onInsertAuthor(newAuthor));
		}

		resetForm();
	};



	let resetForm = () => {
		setAuthorName('');
		setEmail('');
		setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
		setImageFile(null);
		setSelectedId('');
	};



	return (
		<Container>
			<h1 className='my-3'>Home</h1>
				
			<Row>
				<Col md={2}>
				
					<Form>
						<Form.Group>
						<Card style={{ width: '12rem' }}>
							<Card.Img variant="top" src="https://www.kindpng.com/picc/m/364-3647671_software-developer-computer-servers-web-others-software-developer.png"/>
							<Card.Body>
								<Card.Title></Card.Title>
								<Card.Text>
								Please Login
								</Card.Text>
								<Button variant="primary">Login with Google</Button>
							</Card.Body>
						</Card>
							
						</Form.Group>
					</Form>
				</Col>
 
				<Col md={10}>
				<h2>Category</h2>
				<h3>{catagoryName}</h3>
				
				<Row>{articleCard}</Row>			
				</Col>
				
			</Row>	

		</Container>
	);
}

export default Home;



