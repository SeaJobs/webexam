import React, { useEffect, useState } from 'react';
import { Container, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchArticle, onInsertArticle, onDeleteArticle, onUpdateArticle } from '../redux/actions/articleAction';
import { uploadImage } from '../services/article_service';

function Article() {
	const [articleName, setArticleName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');

	// const article = useSelector((state) => state.articles.articles);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchArticle());
	}, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newArticle = {
			name: articleName,
			email,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			newArticle.image = url;
		} else {
			newArticle.image = imageURL;
		}

		if (selectedId) {
			dispatch(onUpdateArticle(selectedId, newArticle));
		} else {
			// insert new author
			dispatch(onInsertArticle(newArticle));
		}

		resetForm();
	};

	const onDelete = (id) => {
		// if selected author was deleted, we reset the form
		if (id === selectedId) resetForm();

		dispatch(onDeleteArticle(id));
	};

	let resetForm = () => {
		setArticleName('');
		setEmail('');
		setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
		setImageFile(null);
		setSelectedId('');
	};

	return (
		<Container>
			<h1 className='my-3'>Add Article</h1>

			<Row>
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>Article Name</Form.Label>
							<Form.Control
								type='text'
								placeholder='Article Name'
								value={articleName}
								onChange={(e) => setArticleName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group controlId='email'>
							<Form.Label>Email</Form.Label>
							<Form.Control
								type='text'
								placeholder='Email'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
                            <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows={3} />
						</Form.Group>

						<Button variant='primary' onClick={onAddOrUpdate}>
							{selectedId ? 'Update' : 'Add'}
						</Button>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>

			
		</Container>
	);
}

export default Article;
