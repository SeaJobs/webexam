import React, { useEffect, useState } from 'react';
import { Container, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { onFetchCatagory, onInsertCatagory, onDeleteCatagory, onUpdateCatagory } from '../redux/actions/catagoryAction';




function Catagory(newCatagory) {
	const [catagoryName, setCatagoryName] = useState('');
	const [selectedId, setSelectedId] = useState('');

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchCatagory());
	}, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();
		if (selectedId) {
			dispatch(onUpdateCatagory(selectedId, newCatagory));
		} else {
			// insert new catagory
			dispatch(onInsertCatagory(newCatagory));
		}

		resetForm();
	};

	const onDelete = (id) => {
		// if selected catagory was deleted, we reset the form
		if (id === selectedId) resetForm();

		dispatch(onDeleteCatagory(id));
	};

	let resetForm = () => {
		setCatagoryName('');
		setSelectedId('');
	};

	return (
		<Container>
			<h1 className='my-3'>Catagory</h1>
			
			<Row>			
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>Catagory Name</Form.Label>
							<Form.Control
								type='text'
								placeholder='Catagory Name'
								value={catagoryName}
								onChange={(e) => setCatagoryName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>
						<Button variant='primary' onClick={onAddOrUpdate}>
							{selectedId ? 'Update' : 'Add'}
						</Button>
					</Form>
				</Col>
				
			</Row>

			
		</Container>
	);
}

export default Catagory;
